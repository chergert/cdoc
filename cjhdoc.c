/* cjhdoc.c
 *
 * Copyright (C) 2014 Christian Hergert <christian@hergert.me>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <clang-c/Index.h>
#include <gio/gio.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct
{
   char *name;
   char *type;
} Param;

typedef struct
{
   const gchar *curfile;
   const gchar *curfunc;

   GPtrArray   *functions;  /* Names of Functions */
   GPtrArray   *structs;    /* Names of Structs */
   GPtrArray   *cflags;     /* CFLAGS arguments */
   gchar       *name;       /* Our guessed name for document */
   GHashTable  *funcbody;   /* Hash of function body text */
   GHashTable  *params;     /* Hash of GPtrArray of Param */
   GHashTable  *functype;   /* Return type of func */
} CjhDoc;

Param *
param_new (CXString     name,
           const gchar *type)
{
   Param *p;

   p = g_new0 (Param, 1);
   p->name = g_strdup (clang_getCString (name));
   p->type = g_strdup (type);

   return p;
}

void
param_free (void *data)
{
   Param *p = data;

   if (p) {
      g_free (p->name);
      g_free (p->type);
      g_free (p);
   }
}

static void
add_param (CjhDoc      *doc,
           const gchar *function,
           Param       *p)
{
   GPtrArray *ar;

   if (!(ar = g_hash_table_lookup (doc->params, function))) {
      ar = g_ptr_array_new_with_free_func (param_free);
      g_hash_table_insert (doc->params, g_strdup (function), ar);
   }

   g_ptr_array_add (ar, p);
}

static CjhDoc *
cjh_doc_new (void)
{
   CjhDoc *doc;

   doc = g_new0 (CjhDoc, 1);
   doc->functions = g_ptr_array_new_with_free_func (g_free);
   doc->structs = g_ptr_array_new_with_free_func (g_free);
   doc->cflags = g_ptr_array_new_with_free_func (g_free);
   doc->funcbody = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
   doc->functype = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
   doc->params = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
                                        (GDestroyNotify)g_ptr_array_unref);

   return doc;
}

static void
print_and_dispose (CXString s)
{
   const gchar *str;

   str = clang_getCString (s);
   g_print ("%s\n", str);
   clang_disposeString (s);
}

static gchar *
get_body (CjhDoc   *doc,
          CXCursor  cursor)
{
   CXSourceRange srange;
   CXSourceLocation begin;
   CXSourceLocation end;
   CXString fname;
   CXFile file;
   gchar *contents = NULL;
   gchar *str = NULL;
   gsize len = 0;
   unsigned begin_off = 0;
   unsigned end_off = 0;

   g_assert (doc);

   srange = clang_getCursorExtent (cursor);
   begin = clang_getRangeStart (srange);
   end = clang_getRangeEnd (srange);

   clang_getExpansionLocation (begin, &file, NULL, NULL, &begin_off);
   clang_getExpansionLocation (end, NULL, NULL, NULL, &end_off);

   fname = clang_getFileName (file);

   if (g_file_get_contents (clang_getCString (fname), &contents, &len, NULL)) {
      str = g_strndup (contents + begin_off, end_off - begin_off);
   }

   g_free (contents);
   clang_disposeString (fname);

   return str;
}

static void
save_body (CjhDoc   *doc,
           CXCursor  cursor)
{
   CXString funcname;
   gchar *name;
   gchar *body;

   funcname = clang_getCursorSpelling (cursor);
   name = g_strdup (clang_getCString (funcname));
   clang_disposeString (funcname);

   if ((body = get_body (doc, cursor))) {
      g_hash_table_replace (doc->funcbody, g_strdup (name), body);
   }

   g_free (name);
}

static void
cjh_doc_free (CjhDoc *doc)
{
   g_ptr_array_free (doc->functions, TRUE);
   g_ptr_array_free (doc->structs, TRUE);
   g_ptr_array_free (doc->cflags, TRUE);
   g_hash_table_unref (doc->funcbody);
   g_hash_table_unref (doc->params);
   g_hash_table_unref (doc->functype);
   g_free (doc->name);
   g_free (doc);
}

static enum CXChildVisitResult
cjh_doc_visit_parmdecl (CXCursor     cursor,
                        CXCursor     parent,
                        CXClientData client_data)
{
   enum CXCursorKind kind;
   CXString s;
   Param *p = client_data;

   kind = clang_getCursorKind (cursor);

   switch ((int)kind) {
   case CXCursor_TypeRef:
      s = clang_getCursorSpelling (cursor);
      g_free (p->type);
      p->type = g_strdup (clang_getCString (s));
      clang_disposeString (s);
      break;
   default:
      break;
   }

   return CXChildVisit_Continue;
}

static enum CXChildVisitResult
cjh_doc_visit_funciondecl (CXCursor     cursor,
                           CXCursor     parent,
                           CXClientData client_data)
{
   enum CXCursorKind kind;
   CjhDoc *doc = client_data;

   g_assert (doc);

   kind = clang_getCursorKind (cursor);

   switch ((int)kind) {
   case CXCursor_TypeRef:
      {
         CXType type;
         CXString xstr;
         char *s;

         type = clang_getCursorType (cursor);
         xstr = clang_getTypeSpelling (type);
         s = g_strdup (clang_getCString (xstr));
         clang_disposeString (xstr);

         g_hash_table_replace (doc->functype, g_strdup (doc->curfunc), s);
      }
      break;
   case CXCursor_ParmDecl:
      {
         CXString s;
         Param *p;

         s = clang_getCursorSpelling (cursor);
         p = param_new (s, NULL);
         clang_disposeString (s);

         clang_visitChildren (cursor,
                              cjh_doc_visit_parmdecl,
                              p);

         add_param (doc, doc->curfunc, p);
      }
      break;
   default:
      g_print ("KIND: %d\n", kind);
      break;
   }

   return CXChildVisit_Continue;
}

static enum CXChildVisitResult
cjh_doc_visit_typedefdecl (CXCursor     cursor,
                           CXCursor     parent,
                           CXClientData client_data)
{
   enum CXCursorKind kind;
   CjhDoc *doc = client_data;

   g_assert (doc);

   kind = clang_getCursorKind (cursor);

   switch ((int)kind) {
   default:
      //g_print (">> ");
      //print_and_dispose (clang_getCursorSpelling (cursor));
      break;
   }

   return CXChildVisit_Continue;
}

static enum CXChildVisitResult
cjh_doc_visit_toplevel (CXCursor     cursor,
                        CXCursor     parent,
                        CXClientData client_data)
{
   CXSourceLocation sloc;
   enum CXCursorKind kind;
   const gchar *cstr;
   gboolean ret;
   CXString str;
   CjhDoc *doc = client_data;
   CXFile file;

   g_assert (doc);

   sloc = clang_getCursorLocation (cursor);
   clang_getExpansionLocation (sloc, &file, NULL, NULL, NULL);
   str = clang_getFileName (file);
   cstr = clang_getCString (str);
   ret = (0 == g_strcmp0 (cstr, doc->curfile));
   clang_disposeString (str);

   if (!ret) {
      return CXChildVisit_Continue;
   }

   kind = clang_getCursorKind (cursor);

   switch ((int)kind) {
   case CXCursor_FunctionDecl:
      //print_and_dispose (clang_getCursorSpelling (cursor));
      str = clang_getCursorSpelling (cursor);
      g_ptr_array_add (doc->functions,
                       g_strdup (clang_getCString (str)));
      save_body (doc, cursor);
      doc->curfunc = clang_getCString (str);
      clang_visitChildren (cursor, cjh_doc_visit_funciondecl, doc);
      doc->curfunc = NULL;
      clang_disposeString (str);
      break;
   case CXCursor_TypedefDecl:
      //print_and_dispose (clang_getCursorSpelling (cursor));
      if (!doc->name) {
         str = clang_getCursorSpelling (cursor);
         doc->name = g_strdup (clang_getCString (str));
         clang_disposeString (str);
      }
      if (0) {
         clang_visitChildren (cursor, cjh_doc_visit_typedefdecl, doc);
      }
      break;
   case CXCursor_EnumDecl:
      //print_and_dispose (clang_getCursorSpelling (cursor));
      break;
   case CXCursor_VarDecl:
      //print_and_dispose (clang_getCursorSpelling (cursor));
      break;
   case CXCursor_StructDecl:
      //print_and_dispose (clang_getCursorSpelling (cursor));
      break;
   default:
      if (0)
         print_and_dispose (clang_getCursorSpelling (cursor));
      break;
   }

   return CXChildVisit_Continue;
}

static void
cjh_doc_parse (CjhDoc      *doc,
               const gchar *filename)
{
   CXTranslationUnit unit = NULL;
   const gchar * const *argv;
   CXIndex idx = NULL;
   gint argc;

   g_assert (doc);
   g_assert (filename);

   g_message ("Parsing %s", filename);

   argv = (const gchar * const *)doc->cflags->pdata;
   argc = doc->cflags->len;

   idx = clang_createIndex (0, 0);
   if (!idx) goto cleanup;

   unit = clang_parseTranslationUnit (idx, filename, argv, argc, NULL, 0, 0);
   if (!unit) goto cleanup;

   doc->curfile = filename;

   clang_visitChildren (clang_getTranslationUnitCursor (unit),
                        cjh_doc_visit_toplevel,
                        doc);

   doc->curfile = NULL;

cleanup:
   if (unit) clang_disposeTranslationUnit (unit);
   if (idx) clang_disposeIndex (idx);
}

#define WRITE_FORMAT(format,...) \
   G_STMT_START { \
      gchar *formatted = g_strdup_printf (format, __VA_ARGS__); \
      g_output_stream_write_all (stream, formatted, strlen (formatted), NULL, NULL, NULL); \
      g_free (formatted); \
   } G_STMT_END

#define WRITE(format) \
   G_STMT_START { \
      g_output_stream_write_all (stream, format, strlen (format), NULL, NULL, NULL); \
   } G_STMT_END

static void
cjh_doc_generate_func (CjhDoc      *doc,
                       const gchar *function)
{
   GOutputStream *stream;
   const gchar *typename;
   const gchar *funcbody;
   gchar *local_funcbody = NULL;
   GFile *file;
   gchar *path;
   gchar *readbuf;
   Param *p;
   GPtrArray *ar;
   unsigned i;

   g_assert (doc);
   g_assert (function);

   ar = g_hash_table_lookup (doc->params, function);

   typename = doc->name ?: "";

   g_message ("Generating %s.page", function);

   path = g_strdup_printf ("%s.page", function);
   file = g_file_new_for_path (path);
   g_free (path);

   stream = (GOutputStream *)g_file_replace (file, NULL, FALSE, G_FILE_CREATE_NONE, NULL, NULL);
   g_assert (stream);

   funcbody = g_hash_table_lookup (doc->funcbody, function);
   if (!funcbody) {
      funcbody = "";
   } else {
      int stdin_fileno;
      int stdout_fileno;
      gchar *argv[] = { "uncrustify", "-c", "uncrustify.cfg", "-l", "C", NULL };
      gsize readbuflen = 2 * strlen (funcbody) + 1;
      GError *error = NULL;
      gsize size;

      readbuf = g_malloc (readbuflen);

      if (!g_spawn_async_with_pipes (".", argv, NULL, G_SPAWN_SEARCH_PATH,
                                     NULL, NULL, NULL, &stdin_fileno,
                                     &stdout_fileno, NULL, &error)) {
         g_printerr ("%s\n", error->message);
         g_clear_error (&error);
         g_error ("Failed to spawn %s", argv [0]);
      }

      write (stdin_fileno, funcbody, strlen (funcbody));
      write (stdin_fileno, ";\n", 2);
      close (stdin_fileno);

      size = read (stdout_fileno, readbuf, readbuflen);
      if (size > 0 && size < readbuflen) {
         readbuf [size] = '\0';
         local_funcbody = strdup (readbuf);
         funcbody = local_funcbody;
      } else g_print ("eek\n");

      close (stdout_fileno);
      g_free (readbuf);
   }

   WRITE_FORMAT ("<?xml version=\"1.0\"?>\n\
<page xmlns=\"http://projectmallard.org/1.0/\"\n\
      type=\"topic\"\n\
      style=\"function\"\n\
      xmlns:api=\"http://projectmallard.org/experimental/api/\"\n\
      xmlns:ui=\"http://projectmallard.org/experimental/ui/\"\n\
      id=\"%s\">\n\
", function);


   WRITE_FORMAT ("  <info>\n\
    <link type=\"guide\" xref=\"%s\" group=\"function\"/>\n\
  </info>\n\
", typename);

   WRITE_FORMAT ("  <title>%s()</title>\n\n", function);

   WRITE_FORMAT ("  <section id=\"synopsis\">\n\
    <title>Synopsis</title>\n\
    <synopsis><code mime=\"text/x-csrc\"><![CDATA[%s]]></code></synopsis>\n\
  </section>\n\n", funcbody);

   if (strstr (funcbody, "DEPRECATED")) {
      WRITE ("\n\
  <section id=\"deprecated\">\n\
    <title>Deprecated</title>\n\
    <p>This function is currently deprecated and should not be used in new code.</p>\n\
  </section>\n\
");
   }

   {
      if (ar && ar->len) {
         WRITE ("  <section id=\"parameters\">\n\
    <title>Parameters</title>\n\
    <table>\n\
");

         for (i = 0; i < ar->len; i++) {
            p = g_ptr_array_index (ar, i);
            WRITE_FORMAT ("      <tr><td><p>%s</p></td><td><p>A %s.</p></td></tr>\n", p->name, p->type);
         }

         WRITE ("    </table>\n\
  </section>\n\
\n\
  <section id=\"description\">\n\
    <title>Description</title>\n\
    <p>TODO:</p>\n\
");

         if (strstr (funcbody, "WARN_UNUSED")) {
            WRITE ("    <note style=\"warning\"><p>Failure to handle the result of this function is a programming error.</p></note>\n\n");
         }

         WRITE ("  </section>\n\n");
      }
   }

   if (strstr (funcbody, "bson_error_t")) {
      WRITE ("  <section id=\"errors\">\n\
    <title>Errors</title>\n\
    <p>Describe errors</p>\n\
  </section>\n\
");
   }

   if (0 != strncmp (funcbody, "void", 4)) {
      WRITE ("  <section id=\"return\">\n\
    <title>Returns</title>\n\
    <p>TODO:</p>\n\
  </section>\n\
");
   }

   WRITE ("</page>\n");

   g_object_unref (stream);
   g_object_unref (file);
   g_free (local_funcbody);
}

static void
cjh_doc_generate (CjhDoc      *doc,
                  const gchar *filename)
{
   GOutputStream *stream = NULL;
   unsigned i;
   gchar **parts = NULL;
   gchar *name = NULL;
   gchar *altered_filename = NULL;
   GFile *file = NULL;

   g_assert (doc);
   g_assert (filename);

   if (!doc->name) {
      parts = g_strsplit (filename, ".", 2);
      name = g_strdup (parts [0]);
      g_strfreev (parts);
   } else {
      name = g_strdup (doc->name);
      altered_filename = g_strdup_printf ("%s.page", name);
      filename = altered_filename;
   }

   g_message ("Generating %s.page", name);

   file = g_file_new_for_path (filename);
   stream = (GOutputStream *)g_file_replace (file, NULL, FALSE, G_FILE_CREATE_NONE, NULL, NULL);
   g_assert (stream);

   WRITE_FORMAT ("<?xml version=\"1.0\"?>\n\
\n\
<page id=\"%s\"\n\
      type=\"guide\"\n\
      style=\"class\"\n\
      xmlns=\"http://projectmallard.org/1.0/\"\n\
      xmlns:api=\"http://projectmallard.org/experimental/api/\"\n\
      xmlns:ui=\"http://projectmallard.org/experimental/ui/\">\n\
  <info>\n\
    <link type=\"guide\" xref=\"index#api-reference\" />\n\
  </info>\n\
  <title>%s</title>\n\
", name, name);

   WRITE ("  <section id=\"description\">\n\
    <title>Synopsis</title>\n\
    <p>Description</p>\n\
  </section>\n\
\n\
");

   if (doc->functions->len) {
      WRITE ("  <links type=\"topic\" groups=\"function\" style=\"2column\">\n");
      WRITE ("    <title>Functions</title>\n");
      WRITE ("  </links>\n");
   }

   WRITE ("</page>\n\
");

   for (i = 0; i < doc->functions->len; i++) {
      cjh_doc_generate_func (doc, g_ptr_array_index (doc->functions, i));
   }

   g_clear_object (&stream);
   g_clear_object (&file);
   g_free (altered_filename);
   g_free (name);
}

int
main (int argc,
      char **argv)
{
   GOptionContext *context;
   GError *error = NULL;
   gboolean found_marker = FALSE;
   CjhDoc *doc = NULL;
   gint ret = EXIT_FAILURE;
   gint i;

   context = g_option_context_new (_(" FILES -- CFLAGS"));

   if (!g_option_context_parse (context, &argc, &argv, &error)) {
      g_printerr ("%s\n", error->message);
      goto cleanup;
   }

   doc = cjh_doc_new ();

   for (i = 1; i < argc; i++) {
      if (found_marker) {
         g_ptr_array_add (doc->cflags, g_strdup (argv [i]));
      } else if (0 == g_strcmp0 (argv [i], "--")) {
         found_marker = TRUE;
      }
   }

   for (i = 1; i < argc; i++) {
      if (0 == g_strcmp0 (argv [i], "--")) {
         break;
      }
      cjh_doc_parse (doc, argv [i]);
   }

   cjh_doc_generate (doc, "outfile.page");

   ret = EXIT_SUCCESS;

cleanup:
   if (doc) {
      cjh_doc_free (doc);
   }
   g_clear_error (&error);
   g_option_context_free (context);

   return ret;
}
