/* cdoc.c
 *
 * Copyright (C) 2014 Christian Hergert <christian.hergert@mongodb.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <clang-c/Index.h>
#include <gio/gio.h>
#include <glib/gi18n.h>
#include <stdlib.h>

typedef struct
{
   const gchar   *filename;
   GOutputStream *stream;
} Visitor;

static void
cdoc_log (const gchar         *opname,
          const gchar         *filename,
          gint                 cflags_len,
          const gchar * const *cflags)
{
   gint i;

   g_print ("cdoc %s %s ", opname, filename);
   for (i = 0; i < cflags_len; i++)
      g_print ("%s ", cflags [i]);
   g_print ("\n");
}

static gchar *
get_cursor_text (const gchar *filename,
                 CXCursor     cursor)
{
   CXSourceRange srange;
   CXSourceLocation sbegin;
   CXSourceLocation send;
   unsigned begin;
   unsigned end;
   CXString cxstr;
   CXFile file;
   gsize length = 0;
   gchar *str = NULL;
   gchar *contents;

   srange = clang_getCursorExtent (cursor);
   sbegin = clang_getRangeStart (srange);
   send = clang_getRangeEnd (srange);

   clang_getExpansionLocation (sbegin, &file, NULL, NULL, &begin);
   clang_getExpansionLocation (send, NULL, NULL, NULL, &end);

   cxstr = clang_getFileName (file);
   if (0 != g_strcmp0 (clang_getCString (cxstr), filename)) {
      clang_disposeString (cxstr);
      return NULL;
   }
   clang_disposeString (cxstr);

   if (g_file_get_contents (filename, &contents, &length, NULL)) {
      if (begin < length && end < length) {
         str = g_strndup (contents + begin, end - begin);
      }
      g_free (contents);
   }

   return str;
}

static enum CXChildVisitResult
cdoc_visitor (CXCursor     cursor,
              CXCursor     parent,
              CXClientData client_data)
{
   enum CXCursorKind kind;
   Visitor *v = client_data;
   GString *s;
   gchar *str;

   kind = clang_getCursorKind (cursor);

   switch (kind) {
   case CXCursor_FunctionDecl:
      str = get_cursor_text (v->filename, cursor);
      if (str) {
         s = g_string_new (NULL);
         g_string_append_printf (s, "<screen><code mime=\"text/x-csrc\"><![CDATA[%s]]></code></screen>\n", str);
         g_output_stream_write_all (v->stream, s->str, s->len, NULL, NULL, NULL);
         g_free (str);
         g_string_free (s, TRUE);
      }
      break;
   default:
      break;
   }

   return CXChildVisit_Recurse;
}

static void
write_header (GOutputStream *stream,
              const gchar   *filename)
{
   GString *s;
   gchar *bname;

   bname = g_path_get_basename (filename);

   s = g_string_new (NULL);
   g_string_append_printf (s, "<page xmlns=\"http://projectmallard.org/1.0/\"\n"
                              "      type=\"topic\"\n"
                              "      id=\"%s\">\n",
                           bname);
   g_free (bname);

   g_string_append (s, "  <info><link type=\"guide\" xref=\"index#api-reference\" /></info>\n");

   g_output_stream_write_all (stream, s->str, s->len, NULL, NULL, NULL);

   g_string_free (s, TRUE);
}

static void
write_footer (GOutputStream *stream,
              const gchar   *filename)
{
   GString *s;

   s = g_string_new (NULL);
   g_string_append_printf (s, "</page>\n");
   g_output_stream_write_all (stream, s->str, s->len, NULL, NULL, NULL);
   g_string_free (s, TRUE);
}

static void
cdoc (const gchar         *filename,
      gint                 cflags_len,
      const gchar * const *cflags)
{
   CXIndex clidx;
   CXTranslationUnit clunit;
   Visitor visitor;
   GFile *f;
   gchar *path;
   gchar *base;

   base = g_path_get_basename (filename);
   path = g_strdup_printf ("%s.page", base);
   g_free (base);

   f = g_file_new_for_path (path);
   g_free (path);

   visitor.stream = (GOutputStream *)g_file_replace (f, NULL, FALSE, G_FILE_CREATE_NONE, NULL, NULL);
   visitor.filename = filename;

   write_header (visitor.stream, filename);

   g_object_unref (f);

   cdoc_log ("analyze", filename, cflags_len, cflags);

   clidx = clang_createIndex (0, 1);
   g_assert (clidx);

   clunit = clang_parseTranslationUnit (clidx, filename, cflags, cflags_len, NULL, 0, 0);
   g_assert (clunit);

   clang_visitChildren (clang_getTranslationUnitCursor (clunit),
                        cdoc_visitor, (CXClientData)&visitor);

   clang_disposeTranslationUnit (clunit);
   clang_disposeIndex (clidx);

   write_footer (visitor.stream, filename);

   g_object_unref (visitor.stream);
}

int
main (gint argc,
      gchar *argv[])
{
   GOptionContext *context;
   GError *error = NULL;
   gchar **cflags = NULL;
   gchar *str;
   gint cflags_len = 0;
   gint i;

   context = g_option_context_new (_("FILES -- CFLAGS"));

   if (!g_option_context_parse (context, &argc, &argv, &error)) {
      g_printerr ("%s\n", error->message);
      g_option_context_free (context);
      g_error_free (error);
      return EXIT_FAILURE;
   }

   if (argc < 2) {
      str = g_option_context_get_help (context, FALSE, NULL);
      g_printerr ("%s", str);
      g_free (str);
      g_option_context_free (context);
      return EXIT_FAILURE;
   }

   for (i = 1; i < argc; i++) {
      if ((0 == strcmp (argv [i], "--")) && ((i + 1) < argc)) {
         cflags = &argv [i + 1];
         cflags_len = argc - i - 1;
      }
   }

   for (i = 1; i < argc; i++) {
      if (0 == strcmp (argv [i], "--")) {
         break;
      }

      cdoc (argv [i], cflags_len, (const gchar * const *)cflags);
   }

   g_option_context_free (context);

   return EXIT_SUCCESS;
}
