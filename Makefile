all: cdoc

PKGS = gio-2.0
WARNINGS = -Wall -Werror
DEBUG = -ggdb
LIBS = $(shell pkg-config --libs $(PKGS)) -L/usr/lib64/llvm -lclang
CFLAGS = $(shell pkg-config --cflags $(PKGS))
OPTIMIZE = -O2

cdoc: cjhdoc.c
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(CFLAGS) $(LIBS) $(OPTIMIZE) $<

clean:
	rm -f cdoc
